import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'dart:io';

class CommonUtils {
  static void showAlertDialog(
      {BuildContext context, String title = "Error occured", String msg}) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(
          title,
          style: TextStyle(
              fontSize: 25,
              color: Colors.black,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold),
        ),
        content: Text(msg),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () => Navigator.of(context).pop(),
          )
        ],
      ),
    );
  }

  static void showToast(BuildContext context, String msg) {
    Toast.show(msg, context, duration: Toast.LENGTH_SHORT);
  }

  static Widget loadImage(File f) {
    return f.existsSync()
        ? Image.file(
            f,
            fit: BoxFit.cover,
          )
        : Image.asset(
            'assets/images/placeholder.png',
            fit: BoxFit.cover,
          );
  }

  static ImageProvider getImageProvider(File f) {
    return f.existsSync()
        ? FileImage(
            f,
//      fit: BoxFit.cover,
          )
        : ExactAssetImage(
            'assets/images/placeholder.png',
          );
  }
}
