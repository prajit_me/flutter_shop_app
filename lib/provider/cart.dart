import 'package:flutter/material.dart';
import './product.dart';

class CartItem {
  final String id;
  final String title;
  int quantity;
  final double price;

  CartItem(this.id, this.title, this.quantity, this.price);
}

class Cart with ChangeNotifier {
  List<CartItem> _carts = [];

  get count {
    var count = 0;
    _carts.forEach((item) => count += item.quantity);
    return count;
  }

  double get total {
    var total = 0.0;
    _carts.forEach((item) => total += item.quantity * item.price);
    return total;
  }

  List<CartItem> get getMyCarts {
    return [..._carts];
  }

  void clearAllCartItem() {
    _carts.clear();
    notifyListeners();
  }

  void addToCart(Product product) {
    final itemIndex = _carts.indexWhere((cart) => cart.id == product.id);
    if (itemIndex > -1) {
      //update the quantity
      _carts[itemIndex].quantity++;
    } else {
      _carts.add(
        CartItem(
          product.id,
          product.productName,
          1,
          product.price,
        ),
      );
    }
    notifyListeners();
  }

  void decreaseCount(String id) {
    final _itemIndex = _carts.indexWhere((cart) => cart.id == id);
    if (_itemIndex > -1) {
      if (_carts[_itemIndex].quantity == 1)
        _carts.removeAt(_itemIndex);
      else {
        _carts[_itemIndex].quantity--;
      }
    }
    notifyListeners();
  }

  void increaseCount(String id) {
    final _itemIndex = _carts.indexWhere((cart) => cart.id == id);
    if (_itemIndex > -1) {
      if (_carts[_itemIndex].quantity == 1)
        _carts.removeAt(_itemIndex);
      else {
        _carts[_itemIndex].quantity++;
      }
    }
    notifyListeners();
  }

  void removeFromCart(String id) {
    final _itemIndex = _carts.indexWhere((cart) => cart.id == id);
    if (_itemIndex > -1) {
      _carts.removeAt(_itemIndex);
      notifyListeners();
    }
  }
}
