import 'package:flutter/material.dart';
import '../provider/cart.dart' as cart;
import 'package:provider/provider.dart';
import '../provider/cart.dart';

class CartItem extends StatelessWidget {
  final cart.CartItem cartItem;

  CartItem(this.cartItem);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(
        cartItem.id,
      ),
      direction: DismissDirection.endToStart,
      background: Container(
        color: Colors.red,
        alignment: Alignment.centerRight,
        child: Icon(
          Icons.delete,
          color: Colors.white,
        ),
      ),
      onDismissed: (direction) =>
          Provider.of<Cart>(context).removeFromCart(cartItem.id),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              backgroundColor: Theme.of(context).accentColor,
              child: FittedBox(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    (cartItem.price * cartItem.quantity).toStringAsFixed(1),
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            title: Text(
              cartItem.title,
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
              ),
            ),
            subtitle: Text(
              'Price: \$${cartItem.price.toStringAsFixed(2)}',
            ),
            trailing: Container(
              width: 130,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.remove,
                    ),
                    onPressed: () =>
                        Provider.of<Cart>(context).decreaseCount(cartItem.id),
                  ),
                  CircleAvatar(
                    radius: 15,
                    backgroundColor: Theme.of(context).primaryColor,
                    child: FittedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          '${cartItem.quantity}',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.add,
                    ),
                    onPressed: () =>
                        Provider.of<Cart>(context).increaseCount(cartItem.id),
                  ),
                ],
              ),
            ),
          ),
          Divider()
        ],
      ),
    );
  }
}
