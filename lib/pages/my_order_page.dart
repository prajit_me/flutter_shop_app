import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/order.dart';
import '../widgets/item_order.dart';
import '../widgets/my_drawer.dart';
import '../helpers/common_utils.dart';

class MyOrderPage extends StatelessWidget {
  static const routeName = 'my-order';

  Future<void> _refreshProduct(BuildContext context) async {
    try {
      await Provider.of<Order>(context, listen: false).getMyOrders();
    } catch (error) {
      CommonUtils.showAlertDialog(context: context, msg: error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My order'),
        ),
        drawer: MyAppDrawer(),
        body: FutureBuilder(
          future: _refreshProduct(context),
          builder: (cntx, snapShot) =>
              snapShot.connectionState == ConnectionState.waiting
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : Consumer<Order>(
                      builder: (cntx, order, _) => order.myOrders.length > 0
                          ? ListView.builder(
                              itemBuilder: (_, index) => OrderItemLayout(
                                order.myOrders[index],
                              ),
                              itemCount: order.myOrders.length,
                            )
                          : Center(
                              child: Text(
                                'Your order is empty. Make order some.',
                              ),
                            ),
                    ),
        ));
  }
}
