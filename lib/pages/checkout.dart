import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/cart.dart';
import '../widgets/cart_item.dart' as Item;
import '../provider/order.dart';
import '../models/http_exception.dart';
import '../helpers/common_utils.dart';

class CheckoutPage extends StatefulWidget {
  static const routeName = 'check-out';

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    var myCarts = Provider.of<Cart>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        title: Text('Checkout'),
      ),
      body: myCarts.getMyCarts.length > 0
          ? Column(
              children: <Widget>[
                Card(
                  margin: EdgeInsets.all(10),
                  elevation: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Total:',
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Chip(
                          label: Text(
                            '\$${myCarts.total.toStringAsFixed(2)}',
                            style: Theme.of(context).textTheme.title,
                          ),
                          backgroundColor: Theme.of(context).accentColor,
                        ),
                        Spacer(),
                        OrderNow(
                          myCarts: myCarts,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (_, index) => Item.CartItem(
                      myCarts.getMyCarts[index],
                    ),
                    itemCount: myCarts.getMyCarts.length,
                  ),
                ),
              ],
            )
          : Center(
              child: Text(
                'Your cart is empty.',
              ),
            ),
    );
  }
}

class OrderNow extends StatefulWidget {
  final Cart myCarts;

  const OrderNow({
    Key key,
    this.myCarts,
  }) : super(key: key);

  @override
  _OrderNowState createState() => _OrderNowState();
}

class _OrderNowState extends State<OrderNow> {
  var _isLoading = false;

  void showLoading() {}

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? CircularProgressIndicator()
        : FlatButton(
            child: Text(
              'Order now',
              style: TextStyle(color: Theme.of(context).primaryColor,
              fontSize: 18,
              fontWeight: FontWeight.bold,),
            ),
            onPressed: () async {
              setState(() {
                _isLoading = true;
              });
              try {
                await Provider.of<Order>(context, listen: false)
                    .addOrder(widget.myCarts.getMyCarts, widget.myCarts.total);

                widget.myCarts.clearAllCartItem();
                CommonUtils.showToast(context, "Order placed successfully.");
              } on HttpException catch (error) {
                CommonUtils.showAlertDialog(
                    context: context, msg: error.message.toString());
              } catch (error) {
                CommonUtils.showAlertDialog(
                    context: context, msg: error.toString());
              }

              setState(() {
                _isLoading = false;
              });
            },
          );
  }
}
