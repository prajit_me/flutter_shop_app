import 'package:flutter/material.dart';
import '../provider/order.dart';
import 'dart:math';

class OrderItemLayout extends StatefulWidget {
  final OrderItem order;

  OrderItemLayout(this.order);
  var _expanded = false;

  @override
  _OrderItemLayoutState createState() => _OrderItemLayoutState();
}

class _OrderItemLayoutState extends State<OrderItemLayout> {
  @override
  Widget build(BuildContext context) {


    return  Card(
        elevation: 5,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                title: Text(
                  '\$${widget.order.total.toStringAsFixed(2)}',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                subtitle: Text(
                  widget.order.dateTime.toIso8601String(),
                ),
                trailing: IconButton(
                  icon: Icon(widget._expanded ? Icons.expand_less : Icons.expand_more),
                  onPressed: () {
                      setState(() {
                        widget._expanded = !widget._expanded;
                      });
                  },
                ),
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 300),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
              height:
              widget._expanded ? min(widget.order.listItem.length * 20.0 + 15, 100) : 0,
              child: ListView(
                children: widget.order.listItem
                    .map(
                      (item) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              item.title,
                            ),
                            Text('\$${item.price.toStringAsFixed(2)}'),
                          ],
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
          ],
        ),
    );
  }
}
