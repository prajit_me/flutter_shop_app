import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './product.dart';

class Products with ChangeNotifier {
  final String authToken;
  final String userId;

  List<Product> _items;

  Products(this.authToken, this.userId, this._items);

  bool isFilterByFavorites = false;

  List<Product> get getItems {
    if (!isFilterByFavorites)
      return [...this._items];
    else {
      List<Product> fliteredList =
          this._items.where((item) => item.isFavorite).toList();
      return fliteredList;
    }
  }

  Future<void> addProduct(Product product) async {
    try {
      final url =
          "https://flutter-shop-app-demo.firebaseio.com/products.json?auth=$authToken";
      final body = json.encode({
        'name': product.productName,
        'price': product.price,
        'description': product.description,
        'imagePath': product.imagePath,
        'creatorId': this.userId
      });

      var response = await http.post(url, body: body);
      print(response.body.toString());
      if (response.statusCode >= 400) {
        throw (response.body.toString());
      } else {
        product.id = json.decode(response.body)['name'];
        _items.add(product);
      }
      return response;
    } catch (error) {
      throw (error);
    }
  }

  Future<void> updateProduct(Product product) async {
    final productIndex = _items.indexWhere((data) => data.id == product.id);
    if (productIndex > -1) {
      final url =
          "https://flutter-shop-app-demo.firebaseio.com/products/${product.id}.json?auth=$authToken";
      final body = json.encode({
        'name': product.productName,
        'price': product.price,
        'description': product.description,
        'imagePath': product.imagePath,
        'creatorId': this.userId
      });

      try {
        var response = await http.put(url, body: body);
        print(response.body.toString());
        if (response.statusCode >= 400) {
          throw (response.body.toString());
        } else {
          _items[productIndex] = product;
        }
        return response;
      } catch (error) {
        throw (error);
      }
    }
  }

  Product getProductById(String productId) {
    return _items.firstWhere((data) => data.id == productId);
  }

  Future<void> fetchProductFromServer({bool isFilterByUser = false}) async {
    isFilterByFavorites = false;
    final filterString =
        isFilterByUser ? 'orderBy="creatorId"&equalTo="$userId"' : '';
    final url =
        'https://flutter-shop-app-demo.firebaseio.com/products.json?auth=$authToken&$filterString';
    try {
      print(url);
      final response = await http.get(url);
      print(response.body.toString());
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      if (extractedData == null) return;
      final List<Product> loadedProduct = [];
      extractedData.forEach((prodId, prodData) {
        loadedProduct.add(
          Product(
            id: prodId,
            productName: prodData['name'],
            price: prodData['price'],
            description: prodData['description'],
            imagePath: prodData['imagePath'],
          ),
        );
      });
      _items = loadedProduct;
      notifyListeners();
    } catch (error) {
      throw (error);
    }
  }

  deleteProduct(Product product) {
    final productPosition = _items.indexWhere((item) => item.id == product.id);
    _items.removeAt(productPosition);
    notifyListeners();
    final url =
        "https://flutter-shop-app-demo.firebaseio.com/products/${product.id}.json?auth=$authToken";

    try {
      http.delete(url);
    } catch (error) {
      _items.insert(productPosition, product);
      notifyListeners();
      throw (error);
    }
  }

  Future<bool> addToFavorite(Product product) async {
    var itemPos = _items.indexWhere((item) => item.id == product.id);
    var originalState = [..._items][itemPos].isFavorite;
    if (itemPos > -1) {
      _items[itemPos].isFavorite = !_items[itemPos].isFavorite;
      notifyListeners();

      if (_items[itemPos].isFavorite) {
        final url =
            "https://flutter-shop-app-demo.firebaseio.com/favourite/$userId/${product.id}.json?auth=$authToken";
        try {
          await http.post(url, body: json.encode({'id': product.id}));
        } catch (error) {
          _items[itemPos].isFavorite = originalState;
          notifyListeners();
          throw (error);
        }
      } else {
        final url =
            "https://flutter-shop-app-demo.firebaseio.com/favourite/${product.id}.json?auth=$authToken";
        try {
          await http.delete(url);
        } catch (error) {
          _items[itemPos].isFavorite = originalState;
          notifyListeners();
          throw (error);
        }
      }
      return _items[itemPos].isFavorite;
    }
    notifyListeners();
    return false;
  }

  bool isFavoriteProduct(Product product) {
    var productItem = _items.firstWhere((item) => item.id == product.id);
    if (productItem != null) return productItem.isFavorite;
    return false;
  }

  void filterProduct(bool isFavorite) {
    isFilterByFavorites = isFavorite;
    notifyListeners();
  }

//TODO Upload images in server
//  Future<String> uploadFile(String imagePath) async {
//    StorageReference storageReference = FirebaseStorage.instance
//        .ref()
//        .child('chats/${Path.basename(imagePath)}}');
//    StorageUploadTask uploadTask = storageReference.putFile(File(imagePath));
//    await uploadTask.onComplete;
//    print('File Uploaded');
//    storageReference.getDownloadURL().then((fileURL) {
//      return fileURL;
//    });
//  }
}
