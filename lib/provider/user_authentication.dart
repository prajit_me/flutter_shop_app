import 'dart:convert';
import '../models/http_exception.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class UserAuthentication with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  String _userId;
  Timer mTimer;

  bool get isAuth {
    return _token != null && _token.isNotEmpty;
  }

  get token {
    return _token;
  }

  get userId {
    return _userId;
  }

  Future<void> _authenticate(
      String email, String password, String urlSegment) async {
    final url =
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/$urlSegment?key=AIzaSyBJ-bUSvS8hrj9eaAr-t_qTK2L-2xwuLi4';
    try {
      var response = await http.post(url,
          body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true,
          }));

      final responseBody = json.decode(response.body);
      print(responseBody.toString());
      if (responseBody['error'] != null) {
        throw HttpException(responseBody['error']['message']);
      }
      _token = responseBody['idToken'];
      _userId = responseBody['localId'];
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: int.parse(
            responseBody['expiresIn'],
          ),
        ),
      );
      notifyListeners();
      final sharedPreferences = await SharedPreferences.getInstance();
      final userData = json.encode({
        'token': _token,
        'userId': _userId,
        'expiresIn': _expiryDate.toIso8601String(),
      });
      sharedPreferences.setString("userData", userData);
    } catch (error) {
      throw error;
    }
  }

  Future<void> login(String userName, String password) {
    return _authenticate(userName, password, "verifyPassword");
  }

  Future<void> register(String userName, String password) {
    return _authenticate(userName, password, "signupNewUser");
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiresIn']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _expiryDate = expiryDate;
    notifyListeners();
    _autoLogout();
    return true;
  }

  void logOut() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    _token = "";
    _expiryDate = null;
    _userId = "";
    notifyListeners();
  }

  void _autoLogout() {
    if (mTimer != null) {
      mTimer.cancel();
    }
    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    mTimer = Timer(Duration(seconds: timeToExpiry), logOut);
  }
}
