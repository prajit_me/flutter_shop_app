import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

import '../helpers/common_utils.dart';
import '../models/http_exception.dart';
import '../provider/product.dart';
import '../provider/products.dart';

class AddEditProducts extends StatelessWidget {
  static const routeName = 'add-edit-products';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('Update product'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: FormKeyboardActions(
        child: AddEditProductsForm(),
      ),
    );
  }
}

class AddEditProductsForm extends StatefulWidget {
  @override
  _AddEditProductsFormState createState() => _AddEditProductsFormState();
}

class _AddEditProductsFormState extends State<AddEditProductsForm> {
  FocusNode _priceFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();
  FocusNode _descriptionFocus = FocusNode();
  File _imageFile;
  var _isInit = false;
  final _formKey = GlobalKey<FormState>();
  var myProduct =
      Product(description: '', imagePath: '', price: 0.0, productName: '');

  Future<void> getImage() async {
    var response = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 600, maxWidth: 600);

    setState(() {
      _imageFile = response;
      myProduct.imagePath = response.path.toString();
    });
  }

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: [
        KeyboardAction(
          focusNode: _nameFocus,
          closeWidget: Padding(
            padding: EdgeInsets.all(5.0),
            child: Text("Done"),
          ),
        ),
        KeyboardAction(
          focusNode: _priceFocus,
          closeWidget: Padding(
            padding: EdgeInsets.all(5.0),
            child: Text("Done"),
          ),
        ),
        KeyboardAction(
          focusNode: _descriptionFocus,
          closeWidget: Padding(
            padding: EdgeInsets.all(5.0),
            child: Text("Done"),
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    FormKeyboardActions.setKeyboardActions(context, _buildConfig(context));
    super.initState();
  }

//  initGalleryPickUp() async {
//    /*setState(() {
//      imageFile = null;
//      _platformVersion = 'Unknown';
//    });*/
//    File file;
//    String result;
//
//    try {
//      result =
//          await FlutterImagePickCrop.pickAndCropImage("fromGalleryCropImage");
//    } on PlatformException catch (e) {
//      result = e.message;
//      print(e.message);
//    }
//
//    if (!mounted) return;
//
//    setState(() {
//      _imageFile = new File(result);
////      _platformMessage = result;
//    });
//  }

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        myProduct = Provider.of<Products>(context).getProductById(productId);
        _imageFile = File(myProduct.imagePath);
      }
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  void _submitForm() async {
    if (!_formKey.currentState.validate()) return;
    _formKey.currentState.save();
    if (myProduct.imagePath.isEmpty) {
      CommonUtils.showAlertDialog(
          context: context,
          title: 'Validation error',
          msg: "Please choose image");
      return;
    }
    var progressDialog = ProgressDialog(context);

    try {
      if (myProduct.id == null) {
        progressDialog.style(message: 'Saving the product. Please wait..');
        progressDialog.show();
        await Provider.of<Products>(context, listen: false)
            .addProduct(myProduct);

        Navigator.of(context).pop();
        Toast.show(
          'Product added successfuly',
          context,
          duration: Toast.LENGTH_SHORT,
          gravity: Toast.BOTTOM,
        );
      } else {
        progressDialog.style(message: 'Updating the product. Please wait..');
        progressDialog.show();
        await Provider.of<Products>(context, listen: false)
            .updateProduct(myProduct);

        Navigator.of(context).pop();
        Toast.show(
          'Product updated successfuly',
          context,
          duration: Toast.LENGTH_SHORT,
          gravity: Toast.BOTTOM,
        );
      }
    } on HttpException catch (error) {
      progressDialog.dismiss();
      CommonUtils.showAlertDialog(context: context, msg: error.message);
    } catch (error) {
      progressDialog.dismiss();
      CommonUtils.showAlertDialog(context: context, msg: error.toString());
    } finally {
      progressDialog.dismiss();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(25),
          child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    initialValue: myProduct.productName,
                    decoration: InputDecoration(
                      labelText: 'Product name',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        gapPadding: 5,
                        borderSide:
                            BorderSide(color: Theme.of(context).accentColor),
                      ),
                    ),
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    focusNode: _nameFocus,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Please provide product name";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      myProduct.productName = value;
                    },
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).requestFocus(_priceFocus),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    initialValue: myProduct.price == 0.0
                        ? ''
                        : myProduct.price.toString(),
                    decoration: InputDecoration(
                      labelText: 'Price',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          gapPadding: 5,
                          borderSide:
                              BorderSide(color: Theme.of(context).accentColor)),
                    ),
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    focusNode: _priceFocus,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please provide price of the product';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      myProduct.price = double.parse(value);
                    },
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).requestFocus(_descriptionFocus),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    initialValue: myProduct.description,
                    decoration: InputDecoration(
                      labelText: 'Description',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          gapPadding: 5,
                          borderSide:
                              BorderSide(color: Theme.of(context).accentColor)),
                    ),
                    maxLines: 3,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please provide description of the product';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    onSaved: (value) {
                      myProduct.description = value;
                    },
                    focusNode: _descriptionFocus,
                    onFieldSubmitted: (value) => _descriptionFocus.unfocus(),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 20, right: 10),
                          child: Divider(
                            height: 3,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                      ),
                      Text(
                        'Add image',
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 10, right: 20),
                          child: Divider(
                            height: 3,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border:
                              Border.all(color: Theme.of(context).accentColor),
                        ),
                        child: _imageFile == null
                            ? Center(
                                child: Text(
                                'No image added',
                                textAlign: TextAlign.center,
                              ))
                            : CommonUtils.loadImage(
                                File(
                                  myProduct.imagePath,
                                ),
                              ),
                      ),
                      FlatButton.icon(
                        icon: Icon(Icons.camera),
                        label: Text("Choose image"),
                        onPressed: () => getImage(),
                      )
                    ],
                  )
                ],
              )),
        ),
      ),
    );
  }
}
