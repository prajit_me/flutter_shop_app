import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Product with ChangeNotifier {
  String id;
  String productName;
  double price;
  String description;
  String imagePath;
  bool isFavorite = false;

  Product(
      {@required this.id,
      @required this.productName,
      @required this.price,
      @required this.description,
      @required this.imagePath});

}
