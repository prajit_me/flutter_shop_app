import './cart.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class OrderItem {
  final String id;
  final DateTime dateTime;
  final double total;
  final List<CartItem> listItem;

  OrderItem({
    @required this.id,
    @required this.dateTime,
    @required this.total,
    @required this.listItem,
  });
}

class Order with ChangeNotifier {
  final String authToken;
  final String userId;
  List<OrderItem> listOrderItem = [];

  Order({this.authToken, this.userId, this.listOrderItem});

  List<OrderItem> get myOrders {
    return [...listOrderItem];
  }

  Future<void> addOrder(List<CartItem> listItem, double total) async {
    final url =
        "https://flutter-shop-app-demo.firebaseio.com/orders/$userId.json?auth=$authToken";
    var timeStamp = DateTime.now();
    try {
      var response = await http.post(
        url,
        body: json.encode(
          {
            "dateTime": timeStamp.toIso8601String(),
            "total": total,
            "products": listItem
                .map((item) => {
                      "id": item.id,
                      "title": item.title,
                      "quantity": item.quantity,
                      "price": item.price
                    })
                .toList()
          },
        ),
      );

      var orderItem = OrderItem(
          id: json.decode(response.body)["name"].toString(),
          dateTime: timeStamp,
          total: total,
          listItem: listItem);
      listOrderItem.insert(0, orderItem);
    } catch (error) {
      throw (error);
    }
  }

  Future<void> getMyOrders() async {
    listOrderItem.clear();
    final url =
        "https://flutter-shop-app-demo.firebaseio.com/orders/$userId.json?auth=$authToken";
    try {
      var response = await http.get(url);
      var orderList = json.decode(response.body) as Map<String, dynamic>;
      // ignore: unrelated_type_equality_checks
      if (orderList == null) return;
      List<OrderItem> loadedList = [];
      orderList.forEach(
        (OrderId, map) => {
          loadedList.add(OrderItem(
              id: OrderId,
              dateTime: DateTime.parse(map['dateTime'].toString()),
              total: map['total'],
              listItem: (map['products'] as List<dynamic>)
                  .map(
                    (item) => CartItem(
                      item['id'],
                      item['title'],
                      item['quantity'],
                      item['price'],
                    ),
                  )
                  .toList())),
        },
      );
      listOrderItem = loadedList;
      notifyListeners();
    } catch (error) {
      throw (error);
    }
  }
}
