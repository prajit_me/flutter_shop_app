import 'package:flutter/material.dart';
import '../provider/product.dart';
import '../provider/products.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import '../pages/add_edit_products.dart';
import '../helpers/common_utils.dart';

class MyProductItem extends StatelessWidget {
  final Product product;

  MyProductItem(this.product);

  void _editProduct(BuildContext context) {
    Navigator.pushNamed(context, AddEditProducts.routeName,
        arguments: product.id);
  }

  bool _confirmDismiss() {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(
        product.id,
      ),
      child: Column(
        children: <Widget>[
          Hero(
            tag: product.productName,
            child: ListTile(
              leading: CircleAvatar(
                radius: 30,
                backgroundImage: CommonUtils.getImageProvider(File(product.imagePath),),
              ),
              title: Text(product.productName),
              subtitle: Text(
                "\$ ${product.price}",
              ),
              onTap: () => _editProduct(context),
            ),
          ),
          Divider(),
        ],
      ),
      direction: DismissDirection.endToStart,
      background: Container(
        color: Colors.red,
        padding: EdgeInsets.all(15),
        child: Icon(
          Icons.delete,
          color: Colors.white,
        ),
        alignment: Alignment.centerRight,
      ),
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text(
              'Delete product',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            content: Text('Are you sure to delete this product?'),
            actions: <Widget>[
              FlatButton(
                child: Text('cancel'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          ),
        );
      },
      onDismissed: (direction) =>
          Provider.of<Products>(context).deleteProduct(product),
    );
  }
}
