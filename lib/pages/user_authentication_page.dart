import 'package:flutter/material.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:provider/provider.dart';

import '../helpers/common_utils.dart';
import '../models/http_exception.dart';
import '../provider/user_authentication.dart';

enum AuthType { LOGIN, REGISTER }

// ignore: must_be_immutable
class UserAuthenticationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: FormKeyboardActions(
        child: Container(
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromRGBO(215, 117, 255, 1).withOpacity(0.5),
                      Color.fromRGBO(255, 188, 117, 1).withOpacity(0.9),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0, 1],
                  ),
                ),
              ),
              SingleChildScrollView(
                  child: Container(
                    width: deviceSize.width,
                    height: deviceSize.height,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/logo.png',
                          width: 250,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Flexible(
                          child: AuthCard(),
                        ),
                      ],
                    ),
                  ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class AuthCard extends StatefulWidget {
  const AuthCard({
    Key key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  @override
  void initState() {
    FormKeyboardActions.setKeyboardActions(context, _buildConfig(context));
    super.initState();
  }

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: [
        KeyboardAction(
          focusNode: _emailAddressFocusNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(5.0),
            child: Text("Done"),
          ),
        ),
        KeyboardAction(
          focusNode: _passwordFocusNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(5.0),
            child: Text("Done"),
          ),
        ),
      ],
    );
  }

  AuthType authType = AuthType.LOGIN;
  GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _passwordController = TextEditingController();
  FocusNode _passwordFocusNode = FocusNode();
  FocusNode _emailAddressFocusNode = FocusNode();
  FocusNode _confirmPasswordFocusNode = FocusNode();
  bool _isLoading = false;

  Map<String, String> _authData = {
    'email': '',
    'password': '',
  };

  void _validateForm() async {
    if (!_formKey.currentState.validate()) return;
    _formKey.currentState.save();

    try {
      setState(() {
        _isLoading = true;
      });
      if (authType == AuthType.LOGIN)
        await Provider.of<UserAuthentication>(context, listen: false)
            .login(_authData['email'], _authData['password']);
      else
        await Provider.of<UserAuthentication>(context, listen: false)
            .register(_authData['email'], _authData['password']);
    } on HttpException catch (error) {
      var errorMessage = "Authentication failed";
      if (error.toString().contains('EMAIL_EXISTS'))
        errorMessage = 'The email address is already in use';
      else if (error.toString().contains('INVALID_EMAIL')) {
        errorMessage = 'This is not a valid email address';
      } else if (error.toString().contains('WEAK_PASSWORD')) {
        errorMessage = 'This password is too weak.';
      } else if (error.toString().contains('EMAIL_NOT_FOUND')) {
        errorMessage = 'Could not find a user with that email.';
      } else if (error.toString().contains('INVALID_PASSWORD')) {
        errorMessage = 'Invalid password.';
      }
      CommonUtils.showAlertDialog(context: context, msg: errorMessage);
    } catch (error) {
      CommonUtils.showAlertDialog(context: context, msg: error.toString());
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    print("state rebuild");
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 40,
      ),
      height: authType == AuthType.REGISTER ? 340 : 280,
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'E-Mail',
                    ),
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    focusNode: _emailAddressFocusNode,
                    onFieldSubmitted: (value) => _fieldFocusChange(
                        context, _emailAddressFocusNode, _passwordFocusNode),
                    validator: (value) {
                      if (value.isEmpty)
                        return 'Please provide email address';
                      else
                        return null;
                    },
                    onSaved: (value) {
                      _authData['email'] = value;
                    },
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(labelText: 'Password'),
                    keyboardType: TextInputType.visiblePassword,
                    controller: _passwordController,
                    focusNode: _passwordFocusNode,
                    onFieldSubmitted: (value) => authType == AuthType.LOGIN
                        ? _validateForm
                        : _fieldFocusChange(context, _passwordFocusNode,
                            _confirmPasswordFocusNode),
                    textInputAction: authType == AuthType.LOGIN
                        ? TextInputAction.send
                        : TextInputAction.next,
                    validator: (value) {
                      if (value.isEmpty)
                        return 'Please provide password';
                      else
                        return null;
                    },
                    onSaved: (value) {
                      _authData['password'] = value;
                    },
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  authType == AuthType.REGISTER
                      ? TextFormField(
                          obscureText: true,
                          decoration:
                              InputDecoration(labelText: 'Confirm Password'),
                          keyboardType: TextInputType.visiblePassword,
                          textInputAction: TextInputAction.send,
                          focusNode: _confirmPasswordFocusNode,
                          onFieldSubmitted: (value) => {
                            _validateForm(),
                            _confirmPasswordFocusNode.unfocus()
                          },
                          validator: (value) {
                            if (value != _passwordController.text)
                              return 'Passwords donot match';
                            else
                              return null;
                          },
                          onSaved: (value) {
                            _authData['password'] = value;
                          },
                        )
                      : SizedBox(),
                  SizedBox(
                    height: 12,
                  ),
                  _isLoading
                      ? CircularProgressIndicator()
                      : RaisedButton(
                          onPressed: _validateForm,
                          color: Colors.purple,
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                              15,
                            ),
                          ),
                          elevation: 0,
                          child: Text(
                            authType == AuthType.LOGIN ? 'LOGIN' : 'SIGN UP',
                          ),
                        ),
                  FlatButton(
                    child: Text(
                      authType == AuthType.LOGIN
                          ? 'SIGNUP INSTEAD'
                          : 'LOGIN INSTEAD',
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ),
                    onPressed: () {
                      if (authType == AuthType.LOGIN)
                        setState(() {
                          authType = AuthType.REGISTER;
                        });
                      else
                        setState(() {
                          authType = AuthType.LOGIN;
                        });
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

_fieldFocusChange(BuildContext mContext, FocusNode _currentFocus, _nextFocus) {
  _currentFocus.unfocus();
  FocusScope.of(mContext).requestFocus(_nextFocus);
}
