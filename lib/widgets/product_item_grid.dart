import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helpers/common_utils.dart';
import '../models/http_exception.dart';
import '../pages/product_detail.dart';
import '../provider/cart.dart';
import '../provider/product.dart';
import '../provider/products.dart';

class ProductItemGrid extends StatelessWidget {
  final Product product;

  ProductItemGrid(this.product);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () =>
//          Navigator.pushNamed(context, ProductDetail.routeName,
//          arguments: product),
          Navigator.push(
        context,
        MaterialPageRoute(
          builder: (ctx) => ProductDetail(product),
          maintainState: true,
        ),
      ),
//          arguments: product),
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Expanded(
              child: Container(
                width: double.infinity,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  child: Hero(
                    tag: product.id,
                    transitionOnUserGestures: false,
                    placeholderBuilder: (_, __, ___) {
                      return CommonUtils.loadImage(
                        File(product.imagePath),
                      );
                    },
                    child: Material(
                      type: MaterialType.transparency,
                      child: CommonUtils.loadImage(
                        File(product.imagePath),
                      ),
                    ),

//                    flightShuttleBuilder: (
//                      BuildContext flightContext,
//                      Animation<double> animation,
//                      HeroFlightDirection flightDirection,
//                      BuildContext fromHeroContext,
//                      BuildContext toHeroContext,
//                    ) {
//                      final Hero toHero = toHeroContext.widget;
//                      return RotationTransition(
//                        turns: animation,
//                        child: toHero.child,
//                      );
//                      return ScaleTransition(
//                        scale: animation.drive(
//                          Tween<double>(begin: 0.0, end: 1.0).chain(
//                            CurveTween(
//                              curve: Interval(0.0, 1.0,
//                                  curve: PeakQuadraticCurve()),
//                            ),
//                          ),
//                        ),
//                        child: toHero.child,
//                      );
//                    },
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Provider.of<Products>(context, listen: true)
                            .isFavoriteProduct(product)
                        ? Icons.favorite
                        : Icons.favorite_border,
                    color: Colors.red,
                  ),
                  onPressed: () async {
                    try {
                      if (!product.isFavorite) {
                        Scaffold.of(context).hideCurrentSnackBar();
                        var snackBar = SnackBar(
                          content: Text(
                            'You like this product',
                          ),
                          duration: Duration(
                            milliseconds: 300,
                          ),
                        );
                        Scaffold.of(context).showSnackBar(snackBar);
                      }
                      await Provider.of<Products>(context, listen: false)
                          .addToFavorite(product);
                    } on HttpException catch (error) {
                      Scaffold.of(context).hideCurrentSnackBar();
                      var snackBar = SnackBar(
                        content: Text(error.message),
                        duration: Duration(
                          milliseconds: 300,
                        ),
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                    } catch (error) {
                      Scaffold.of(context).hideCurrentSnackBar();
                      var snakBar = SnackBar(content: Text(error.message));
                      Scaffold.of(context).showSnackBar(snakBar);
                    }
                  },
                ),
                Expanded(
                  child: Text(
                    product.productName,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.shopping_cart,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    Provider.of<Cart>(context, listen: false)
                        .addToCart(product);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class PeakQuadraticCurve extends Curve {
  @override
  double transform(double t) {
    assert(t >= 0.0 && t <= 1.0);
    return -15 * math.pow(t, 2) + 15 * t + 1;
  }
}
