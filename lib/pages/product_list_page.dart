import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/products.dart';
import '../provider/cart.dart';
import '../widgets/my_drawer.dart';
import '../widgets/product_item_grid.dart';
import '../pages/checkout.dart';

class ProductListPage extends StatelessWidget {
  static const routeName = 'product-list';

  Future<void> _fetchProducts(BuildContext context) async {
    return await Provider.of<Products>(context, listen: false)
        .fetchProductFromServer(isFilterByUser: false);
  }

  void _filterProduct(BuildContext context, bool isFavorite) async {
    return await Provider.of<Products>(context, listen: false)
        .filterProduct(isFavorite);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyAppDrawer(),
      appBar: AppBar(
        title: Text(
          'MyShop',
          style: Theme.of(context).textTheme.title,
        ),
        actions: <Widget>[
          ShoppingCartItem(),
          PopupMenuButton<FilterOptions>(
            onSelected: (FilterOptions result) {
              if (result == FilterOptions.MY_FAVOURITES) {
                _filterProduct(context, true);
              } else {
                _filterProduct(context, false);
              }
            },
            itemBuilder: (BuildContext context) =>
                <PopupMenuEntry<FilterOptions>>[
              const PopupMenuItem<FilterOptions>(
                value: FilterOptions.MY_FAVOURITES,
                child: Text('Favorite'),
              ),
              const PopupMenuItem<FilterOptions>(
                value: FilterOptions.ALL,
                child: Text('All products'),
              )
            ],
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () => _fetchProducts(context),
        child: FutureBuilder(
          future: _fetchProducts(context),
          builder: (cntx, snapShots) =>
//              snapShots.connectionState == ConnectionState.waiting
//                  ? Center(
//                      child: CircularProgressIndicator(),
//                    )
//                  :
              Consumer<Products>(
            builder: (context, _productData, _) => Padding(
              padding: const EdgeInsets.all(5),
              child: _productData.getItems == null ||
                      _productData.getItems.isEmpty
                  ? Center(
                      child:
                          snapShots.connectionState == ConnectionState.waiting
                              ? CircularProgressIndicator()
                              : Text("No products available."),
                    )
                  : GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                      ),
                      itemBuilder: (cntx, index) => ProductItemGrid(
                        _productData.getItems[index],
                      ),
                      itemCount: _productData.getItems.length,
                    ),
            ),
          ),
        ),
      ),
    );
  }
}

class ShoppingCartItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<Cart>(
      builder: (_, cart, __) => Stack(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              if (cart.count > 0)
                Navigator.of(context).pushNamed(CheckoutPage.routeName);
            },
          ),
          cart.count > 0
              ? Positioned(
                  top: 3,
                  right: 4,
                  child: CircleAvatar(
                    radius: 10,
                    backgroundColor: Colors.blue,
                    child: FittedBox(
                      child: Text(
                        cart.count.toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ),
                )
              : Text('')
        ],
      ),
    );
  }
}

enum FilterOptions { MY_FAVOURITES, ALL }
