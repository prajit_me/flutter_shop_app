import 'package:flutter/material.dart';
import '../provider/user_authentication.dart';
import 'package:provider/provider.dart';
import '../pages/my_products.dart';
import '../pages/my_order_page.dart';

class MyAppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: 320,
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
            height: 150,
            color: Theme.of(context).primaryColor,
            child: Center(
              child: Text(
                'Welcome to my shop',
                style: Theme.of(context).textTheme.title,
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.shop),
            title: Text('Shop'),
            onTap: () => Navigator.of(context).pushReplacementNamed('/'),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.credit_card),
            title: Text('Orders'),
            onTap: () => Navigator.pushReplacementNamed(context, MyOrderPage.routeName),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Manage products'),
            onTap: () => Navigator.of(context)
                .pushReplacementNamed(MyProductsPage.routeName),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Log out'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushReplacementNamed('/');
              Provider.of<UserAuthentication>(context, listen: false).logOut();
            },
          ),
        ],
      ),
    );
  }
}
