import 'package:flutter/material.dart';
import '../provider/product.dart';
import 'dart:io';
import '../helpers/common_utils.dart';

class ProductDetail extends StatelessWidget {
  static const routeName = 'product-detail';
  final Product product;

  ProductDetail(this.product);

  @override
  Widget build(BuildContext context) {
//    var product = ModalRoute.of(context).settings.arguments as Product;
    return Scaffold(
      body: CustomScrollView(
        key: GlobalKey(),
        slivers: <Widget>[
          SliverAppBar(
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                product.productName,
              ),
              centerTitle: false,
              background: Hero(
                tag: product.id,
                transitionOnUserGestures: false,
                child: CommonUtils.loadImage(
                  File(product.imagePath),
                ),
              ),
            ),
            expandedHeight: 250,
            pinned: true,
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox(
                  height: 20,
                ),
                Chip(
                  label: Text(
                    'Price: \$${product.price}',
                    style: Theme.of(context).textTheme.title,
                  ),
                  backgroundColor: Theme.of(context).accentColor,
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Description',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 4,
                    horizontal: 16,
                  ),
                  child: Text(
                    product.description,
                    style: TextStyle(),
                  ),
                ),
                SizedBox(
                  height: 400,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
