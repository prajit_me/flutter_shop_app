import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './helpers/custom_route.dart';
import './pages/add_edit_products.dart';
import './pages/my_order_page.dart';
import './pages/my_products.dart';
import './pages/product_list_page.dart';
import './pages/splash_screen.dart';
import './pages/user_authentication_page.dart';
import './provider/cart.dart';
import './provider/order.dart';
import './provider/products.dart';
import './provider/user_authentication.dart';
import 'pages/checkout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: UserAuthentication(),
        ),
        //For multiple dependency
        ChangeNotifierProxyProvider<UserAuthentication, Products>(
          builder: (cntx, auth, previousProvider) => Products(
            auth.token,
            auth.userId,
            previousProvider == null ? [] : previousProvider.getItems,
          ),
        ),
        ChangeNotifierProvider.value(
          value: Cart(),
        ),
        ChangeNotifierProxyProvider<UserAuthentication, Order>(
          builder: (_, auth, previousProvider) => Order(
              authToken: auth.token,
              userId: auth.userId,
              listOrderItem:
                  previousProvider == null ? [] : previousProvider.myOrders),
        ),
      ],
      child: Consumer<UserAuthentication>(
        builder: (ctx, auth, _) => MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primaryColor: Color(0xffEF444E),
            accentColor: Color(0xffea7e84),
            splashColor: Colors.grey,
            fontFamily: "Lato",
            pageTransitionsTheme: PageTransitionsTheme(builders: {
              TargetPlatform.iOS: CustomPageTransitionBuilder(),
              TargetPlatform.android: CustomPageTransitionBuilder(),
            }),
            textTheme: TextTheme(
              title: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontStyle: FontStyle.normal,
                fontFamily: "Lato",
              ),
              body1: TextStyle(
                color: Colors.blueGrey,
                fontSize: 16,
              ),
              body2: TextStyle(
                color: Colors.black,
                fontSize: 14,
              ),
            ),
          ),
          home: auth.isAuth
              ? ProductListPage()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, resultSnapShot) =>
                      resultSnapShot.connectionState == ConnectionState.waiting
                          ? SplashScreen()
                          : UserAuthenticationPage(),
                ),
          routes: {
            ProductListPage.routeName: (context) => ProductListPage(),
            MyProductsPage.routeName: (context) => MyProductsPage(),
            AddEditProducts.routeName: (context) => AddEditProducts(),
            CheckoutPage.routeName: (context) => CheckoutPage(),
            MyOrderPage.routeName: (context) => MyOrderPage(),
//            ProductDetail.routeName: (context) => ProductDetail(),
          },
        ),
      ),
    );
  }
}
