import 'package:flutter/material.dart';
import '../widgets/my_drawer.dart';
import '../pages/add_edit_products.dart';
import '../widgets/product_item_list.dart';
import 'package:provider/provider.dart';
import '../provider/products.dart';

class MyProductsPage extends StatelessWidget {
  static const routeName = 'my-products';

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<Products>(context, listen: false)
        .fetchProductFromServer(isFilterByUser: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyAppDrawer(),
      appBar: AppBar(
        title: Text('My products'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () =>
                Navigator.of(context).pushNamed(
                  AddEditProducts.routeName,
                ),
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () => _refreshProducts(context),
        child: FutureBuilder(
          future: _refreshProducts(context),
          builder: (context, snapShot) =>
          snapShot.connectionState == ConnectionState.waiting
              ? Center(
            child: CircularProgressIndicator(),
          )
              : Consumer<Products>(
            builder: (context, _productData, _) =>
            _productData.getItems == null ||
                _productData.getItems.isEmpty
                ? Center(
              child: Text("No data available."),
            )
                : ListView.builder(
              itemBuilder: (context, index) =>
                  Column(
                    children: <Widget>[
                      MyProductItem(
                        _productData.getItems[index],
                      ),
                    ],
                  ),
              itemCount: _productData.getItems.length,
            ),
          ),
        ),
      ),
    );
  }
}

